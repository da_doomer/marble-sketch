# marble-sketch

Marble simulator implemented in sketch.

![Marble GIF](doc/README.gif)

## Instructions

1. Clone the repository:

```
git clone --recurse-submodules https://gitlab.com/da_doomer/marble-sketch.git
```

1. Install the following dependencies:

```
openjdk maven python3.9 python-poetry
```

2. Execute in a shell:

```
$ make
```
