"""Plot a simulation JSON."""
from pathlib import Path
import argparse
import shutil
import jsons
from PIL import Image
from PIL import ImageDraw
import ffmpeg
from simulator.simulation import Simulation


def plot(
        simulations: list[Simulation],
        output_mp4: Path,
        video_width: int,
        video_height: int,
        background_color: str,
        fps: int,
        circle_fill_color: str,
        segment_fill_color: str,
        goal_fill_color: str,
        ):
    """Plot the given list of simulation states to the output path."""
    # Create a directory to store all frames
    frame_dir = output_mp4.with_suffix("")
    frame_dir.mkdir(exist_ok=False)

    # Store frame by frame
    for i, simulation in enumerate(simulations):
        img = Image.new(
            mode="RGB",
            size=(video_width, video_height),
            color=background_color,
        )
        simulation.draw_onto(
            img=img,
            circle_fill_color=circle_fill_color,
            segment_fill_color=segment_fill_color,
            goal_fill_color=goal_fill_color,
        )
        frame_path = frame_dir/(f"{i}.png").rjust(10)
        img.save(frame_path, "PNG")

    (
        ffmpeg
        .input(frame_dir/"*.png", pattern_type="glob", framerate=fps)
        .output(str(output_mp4))
        .overwrite_output()
        .run(quiet=True)
    )
    shutil.rmtree(frame_dir)


def main():
    """Command-line executable method."""
    parser = argparse.ArgumentParser(
            description='Plot a simulation JSON to an output file.'
        )
    parser.add_argument(
            '--json',
            help="Input JSON containing a list of simulation states.",
            type=Path,
        )
    parser.add_argument(
            '--mp4',
            help="Output MP4 file.",
            type=Path,
        )
    parser.add_argument(
            '--video_width',
            help="Width of the MP4 file.",
            type=int,
            default=800,
        )
    parser.add_argument(
            '--video_height',
            help="Height of the MP4 file.",
            type=int,
            default=600,
        )
    parser.add_argument(
            '--video_fps',
            help="Height of the MP4 file.",
            type=int,
            default=60,
        )
    parser.add_argument(
            '--circle_fill_color',
            help="Circle fill color.",
            type=str,
            default="#0000ff",
        )
    parser.add_argument(
            '--goal_fill_color',
            help="Circle fill color.",
            type=str,
            default="#f7e5e5",
        )
    parser.add_argument(
            '--segment_fill_color',
            help="Segment fill color.",
            type=str,
            default="#000000",
        )
    parser.add_argument(
            '--background_color',
            help="Background color.",
            type=str,
            default="#ffffff",
        )
    args = parser.parse_args()

    # Open JSON file
    with open(args.json, "rt") as f_p:
        json_str = "\n".join(f_p)
    simulations = jsons.loads(
            json_str,
            cls=list[Simulation],
        )
    plot(
        simulations=simulations,
        output_mp4=args.mp4,
        video_width=args.video_width,
        video_height=args.video_height,
        fps=args.video_fps,
        circle_fill_color=args.circle_fill_color,
        segment_fill_color=args.segment_fill_color,
        goal_fill_color=args.goal_fill_color,
        background_color=args.background_color,
    )


if __name__ == "__main__":
    main()
