"""Simulation objects for plotting."""
from dataclasses import dataclass
from .segment import Segment
from .circle import DynamicCircle
from .goal import Goal
from .vec2d import Vec2d
from .drawable import Drawable
from PIL import Image


@dataclass
class Simulation(Drawable):
    """Simulation state."""
    segments: list[Segment]
    circle: DynamicCircle
    goal: Goal

    def draw_onto(
            self,
            img: Image,
            circle_fill_color: str,
            segment_fill_color: str,
            goal_fill_color: str,
            ):
        xs = list()
        for s in self.segments:
            xs.append(s.p1.x)
            xs.append(s.p2.x)
        xs.append(self.circle.position.x+self.circle.radius)
        xs.append(self.circle.position.x-self.circle.radius)
        xs.append(self.goal.position.x+self.goal.radius)
        xs.append(self.goal.position.x-self.goal.radius)
        ys = list()
        for s in self.segments:
            ys.append(s.p1.y)
            ys.append(s.p2.y)
        ys.append(self.circle.position.y+self.circle.radius)
        ys.append(self.circle.position.y-self.circle.radius)
        ys.append(self.goal.position.y+self.goal.radius)
        ys.append(self.goal.position.y-self.goal.radius)

        xs = map(abs, xs)
        max_x = max(xs)
        max_y = max(ys)
        scale_x = img.width/2/max_x
        scale_y = img.height/2/max_y
        scale = min(scale_x, scale_y)
        self.circle.draw_onto(
            img=img,
            fill_color=circle_fill_color,
            scale=scale
        )
        self.goal.draw_onto(
            img=img,
            fill_color=goal_fill_color,
            scale=scale,
        )
        for segment in self.segments:
            segment.draw_onto(
                img=img,
                fill_color=segment_fill_color,
                scale=scale,
            )
