"""Abstract Base Class for drawable simulation objects."""
from abc import ABC
from abc import abstractmethod
from PIL import Image


class Drawable(ABC):
    """A simulation object that can be drawn onto a pillow ImageDraw."""
    @abstractmethod
    def draw_onto(self, img: Image, scale: float):
        """Draw the shape onto the given image."""

    @staticmethod
    def coords_to_img_coords(
            coords: tuple[float, float],
            img: Image,
            scale: float
            ):
        """Translate the coordinates to image reference frame. Scale controls
        how many pixels each unit represents."""
        scaled_coords = (
            coords[0]*scale,
            coords[1]*scale,
        )

        # Shift the origin to the image center
        translated_coords = (
            scaled_coords[0]+img.width/2,
            -scaled_coords[1]+img.height/2,
        )
        return translated_coords
