"""Circle objects for plotting."""
from dataclasses import dataclass
from PIL import Image
from PIL import ImageDraw
from .vec2d import Vec2d
from .drawable import Drawable


@dataclass
class DynamicCircle(Drawable):
    """A dynamic circle."""
    position: Vec2d
    velocity: Vec2d
    radius: float

    def draw_onto(
            self,
            img: Image,
            fill_color: str,
            scale: float,
            ):
        bounding_box = [
            (
                self.position.x - self.radius,
                self.position.y + self.radius,
            ), (
                self.position.x + self.radius,
                self.position.y - self.radius,
            ),
        ]
        transformed_bounding_box = [
            self.coords_to_img_coords(
                coords=coords,
                img=img,
                scale=scale,
            )
            for coords in bounding_box
        ]
        draw = ImageDraw.Draw(img)
        draw.ellipse(
            transformed_bounding_box,
            fill=fill_color
        )
