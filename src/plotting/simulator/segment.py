"""Segment objects for plotting."""
from dataclasses import dataclass
from PIL import Image
from PIL import ImageDraw
from .vec2d import Vec2d
from .drawable import Drawable


@dataclass
class Segment(Drawable):
    """Two-dimensional line segment."""
    p1: Vec2d
    p2: Vec2d
    radius: float

    def draw_onto(
            self,
            img: Image,
            fill_color: str,
            scale: float,
            ):
        p1, p2 = [
            self.coords_to_img_coords(
                coords=(p.x, p.y),
                img=img,
                scale=scale,
            )
            for p in [self.p1, self.p2]
        ]
        draw = ImageDraw.Draw(img)
        draw.line(
            [p1, p2],
            fill=fill_color,
            width=int(self.radius*scale),
        )
