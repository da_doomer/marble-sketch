"""Vec2d objects for plotting."""
from dataclasses import dataclass


@dataclass
class Vec2d:
    """Two-dimensional vector."""
    x: float
    y: float
