# Source code
SRC = src
SIMULATOR_SRC = $(SRC)/simulator
SIMULATORSK = $(SIMULATOR_SRC)/main.sk
SIMULATOR_OUTPUT_CPP = main.cpp
SIMULATOR_OUTPUT_H = main.h
PLOTTING_SRC = $(SRC)/plotting
PLOTTING_PY = $(PLOTTING_SRC)/simulations.py

# Sketch
LIB = lib
SKETCH_COMPILED_FILE = .sketch
SKETCH_FRONTEND_DIR = $(LIB)/sketch-frontend
SKETCH_BACKEND_DIR = $(LIB)/sketch-backend
SKETCH = $\
	java -cp $(SKETCH_FRONTEND_DIR)/target/sketch-*-all-*.jar $\
	-ea sketch.compiler.main.seq.SequentialSketchMain $\
	--fe-inc $(SIMULATOR_SRC) $\
	--fe-inc $(SKETCH_FRONTEND_DIR)/src/sketchlib $\
	--fe-output-code $\
	-V 2

BUILD = build

# Python
PYTHON = poetry run python

# Build
CC = g++ -I $(SKETCH_FRONTEND_DIR)/src/runtime/include
SIMULATORCPP = $(BUILD)/$(SIMULATOR_OUTPUT_CPP)
SIMULATORH = $(BUILD)/$(SIMULATOR_OUTPUT_H)
SIMULATOR = $(BUILD)/sketch
SIMULATOR_OUTPUT = $(BUILD)/simulation.json
PLOTTING_ANIMATION = $(BUILD)/simulation.mp4

.PHONY: run default clean poetry_install sketch

default: $(PLOTTING_ANIMATION)

$(PLOTTING_ANIMATION): $(SIMULATOR_OUTPUT) $(PLOTTING_PY) poetry_install
	$(PYTHON) $(PLOTTING_PY) --json $(SIMULATOR_OUTPUT) --mp4 $(PLOTTING_ANIMATION)

$(SIMULATOR_OUTPUT): $(SIMULATOR)
	$(SIMULATOR) | tee $(SIMULATOR_OUTPUT)

$(SIMULATOR): $(SIMULATORCPP) $(SIMULATORH)
	$(CC) -o $@ $< -I $(SKETCHDIR)/sketch-frontend/runtime/include -I $(LIB)

$(SIMULATORCPP) $(SIMULATORH): $(SKETCH_COMPILED_FILE) $(SIMULATORSK) $(SIMULATOR_SRC)/*sk $(SIMULATOR_SRC)/simulator/*sk
	$(SKETCH) $(SIMULATORSK)
	echo "int main() { ANONYMOUS::main__Wrapper(); return 0; }" >> $(SIMULATOR_OUTPUT_CPP)
	mkdir -p $(BUILD)
	mv $(SIMULATOR_OUTPUT_CPP) $(SIMULATORCPP)
	mv $(SIMULATOR_OUTPUT_H) $(SIMULATORH)

$(SKETCH_COMPILED_FILE):
	cd $(SKETCH_BACKEND_DIR) && bash autogen.sh
	cd $(SKETCH_BACKEND_DIR) && ./configure
	cd $(SKETCH_BACKEND_DIR) && make
	cd $(SKETCH_FRONTEND_DIR) && make assemble-arch
	touch $(SKETCH_COMPILED_FILE)

poetry_install:
	poetry install

clean:
	$(RM) -R build
	$(RM) $(SKETCH_COMPILED_FILE)
